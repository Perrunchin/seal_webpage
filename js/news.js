// Global Variables
window.onload = function() 
{
	setMonths();

	// Set Content size
	var content_height = document.getElementById("content-info").offsetHeight;
	var links_height = document.getElementById("links-info").offsetHeight;

	if (content_height > links_height)
	{
		document.getElementById("links-info").style.height = (content_height - 10) + "px";
	}
	else
	{
		document.getElementById("content-info").style.height = (links_height - 50) + "px";
	}
};

function setMonths() 
{
	var content = document.getElementById("content-info");
	var news = document.getElementsByClassName("news");

	for (var i = 0; i < news.length; i++)
	{
		var month = news[i].getElementsByClassName("month")[0];
		var monthWidth = month.getBoundingClientRect().width;
		var monthHeight = month.getBoundingClientRect().height;

		var monthContainer = news[i].getElementsByClassName("month-container")[0];
		var monthContainerTop = monthContainer.getBoundingClientRect().top;
		alert(monthContainerTop);

		var list = news[i].getElementsByClassName("list")[0];
		var listHeight = list.getBoundingClientRect().height;

		var ul = list.getElementsByTagName("ul")[0];
		var ulHeight = ul.getBoundingClientRect().height;

		var padding = listHeight - ulHeight;
		monthHeight += padding;

		if (monthHeight > listHeight)
		{
			list.style.height = (monthHeight - padding) + "px";
			monthContainer.style.height = monthHeight + "px";

			ul.style.marginTop = ((monthHeight - padding) - ulHeight) / 2 + "px";
		}
		else
		{
			month.style.height = listHeight + "px";
			monthContainer.style.height = listHeight + "px";
		}
	}
}
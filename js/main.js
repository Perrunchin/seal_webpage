// Global Variables
window.onload = function() 
{
    // Put footer in the bootom
    var body = document.body;
    var html = document.documentElement;
    var document_height = body.offsetHeight;

	// Set Content size
	var content_height = document.getElementById("content-id").offsetHeight;
	var links_height = document.getElementById("links-id").offsetHeight;

	if (content_height > links_height)
	{
		document.getElementById("links-id").style.height = (content_height - 10) + "px";
		links_height = content_height - 10;
	}
	else
	{
		document.getElementById("content-id").style.height = (links_height - 30) + "px";
		content_height = links_height - 30;
	}

    // Fill space
    var header_height = document.getElementById("header-id").offsetHeight;
    var terms_height = document.getElementById("terms-id").offsetHeight;

    if ((content_height + header_height < document_height) && (links_height + header_height < document_height))
    {
        var links_rect = document.getElementById("links-id").getBoundingClientRect();
        document.getElementById("links-id").style.height = (document_height - links_rect.top - terms_height - 10) + "px";
        document.getElementById("content-id").style.height = (document_height - links_rect.top - terms_height - 30) + "px";
    }
};

function navigationHover(element)
{
	document.getElementById("links-id").style.display = "inline";
	element.style.width = document.getElementById("links-id").style.width + "px";

	document.getElementById("links-id").style.position = "absolute";

	var rect = element.getBoundingClientRect();
	alert(rect.top);
}

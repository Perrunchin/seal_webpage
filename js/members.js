window.onload = function () 
{
	// Get elements
	var content = document.getElementById("content-id");
	var links = document.getElementById("links-id");

	// Set Content size
	var content_height = content.offsetHeight;
	var links_height = links.offsetHeight;

	if (content_height > links_height)
	{
		document.getElementById("links-id").style.height = (content_height - 10) + "px";
	}
	else
	{
		document.getElementById("content-id").style.height = (links_height - 50) + "px";
	}

	// Put black square
	for (var i = 0; i < content.getElementsByClassName("frame").length; i++)
	{
		var frameRect = content.getElementsByClassName("frame")[i].getBoundingClientRect();
		content.getElementsByClassName("frame")[i].style.height = frameRect.height - 4 + "px";
	}

    //// Put footer in the bootom
    //var body = document.body;
    //var html = document.documentElement;
    //var document_height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);

    //// Fill space
    //var terms_height = document.getElementById("terms-id").offsetHeight;

    //if ((content_height > document_height) || (links_height > document_height))
    //{

    //}
    //else
    //{
    //    var links_rect = document.getElementById("links-id").getBoundingClientRect();
    //    document.getElementById("links-id").style.height = (document_height - links_rect.top - terms_height - 10) + "px";
    //    document.getElementById("content-id").style.height = (document_height - links_rect.top - terms_height - 30) + "px";
    //}
};
